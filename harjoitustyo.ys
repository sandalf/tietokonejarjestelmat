main:                       #Init variables in preparation
    irmovq $0x1000, %rsp    #Init stack pointer
    irmovq $0x1000, %rbp    #Init stack start address
    irmovq $0x1200, %r14    #Memory position variable
    irmovq $1, %r12         #Symbol string length test variable
    subq %r10, %r12         #Test symbol string length
    je oneSymbol            #Jump if there's only one symbol
    rrmovq %r10, %r11       #Loop variable
    irmovq $1, %r12         #Loop decrement variable
    subq %r12, %r11         #Decrement loop variable
    irmovq $8, %r13         #Memory move variable

newSymbol:                  #Init variables when last symbol was new
    mrmovq (%r14), %rcx     #Read symbol from memory
    irmovq $1, %rdx         #Init same symbol counter
    rrmovq %rcx, %rsi       #Init test variable

sameSymbol:                 #Init variables for next symbol to test equality
    addq %r13, %r14         #Move to next memory slot
    mrmovq (%r14), %rbx     #Read symbol from memory
    rrmovq %rbx, %rdi       #Init test variable
    subq %r12, %r11         #Decrement loop variable
    subq %rsi, %rdi         #Test equality
    jne notEqual            #Jump if symbols aren't equal

    #Equal
equal:                      #Tested symbols are equal
    addq %r12, %rdx         #Increment same symbol counter
    subq %r8, %r11          #Test if last symbol
    jne sameSymbol          #If not continue looping through memory
    pushq %rdx              #Push number of same symbols to stack
    pushq %rcx              #Push symbol to stack
    jmp dcss                #No more symbols, count DCSS

    #Not Equal
notEqual:                   #Tested symbols are not equal
    addq %r12, %r9          #Increment number of bytes counter
    pushq %rdx              #Push number of same symbols to stack
    pushq %rcx              #Push symbol to stack
    subq %r8, %r11          #Test if last symbol
    jne newSymbol           #If not continue looping through memory
    pushq %r12              #Push number of same symbols to stack
    pushq %rbx              #Push symbol to stack
    #jmp dcss, no need to jump but just for clarity

dcss:                       #Count data compression space saving
    addq %r12, %r9          #Increment number of bytes counter
    addq %r9, %r9           #Double it to get the correct number
    #dcss = 100 * (1 - %r9 / %r10) = 100 - 100 * %r9 / %r10
    #100 * %r9 = 64 * %r9 + 32 * %r9 + 4 * %r9
    addq %r9, %r9           #%r9 = 2 * %r9
    addq %r9, %r9           #%r9 = 4 * %r9
    rrmovq %r9, %rsi        #%rsi = 4 * %r9
    addq %r9, %r9           #%r9 = 8 * %r9
    addq %r9, %r9           #%r9 = 16 * %r9
    addq %r9, %r9           #%r9 = 32 * %r9
    rrmovq %r9, %rdi        #%rdi = 32 * %r9
    addq %r9, %r9           #%r9 = 64 * %r9
    addq %rdi, %r9          #%r9 = 64 * %r9 + 32 * %r9 = 96 * %r9
    addq %rsi, %r9          #%r9 = 96 * %r9 + 4 * %r9 = 100 * %r9
    rrmovq %r10, %r11       #Loop variable
    irmovq $-1, %r13        #Division loop counter variable

division:                   #Count 100 * %r9 / %r10, store result to %r13
    addq %r12, %r13         #Increment division loop counter
    subq %r10, %r9          #Decrement remainder
    jge division            #Jump until remainder < 0
    irmovq $100, %rsi       #Init variable for 100 - %r13
    subq %r13, %rsi         #%rsi = 100 - %r13
    rrmovq %rsi, %rax       #Store DCSS's result to %rax
    halt                    #Everything is done, stop program

oneSymbol:
    pushq %r10              #Push number of same symbols to stack (=1)
    mrmovq (%r14), %rcx     #Read symbol from memory
    pushq %rcx              #Push symbol to stack
    irmovq $-100, %rax      #Store DCSS's result (=always -100) to %rax
    halt                    #Everything is done, stop program
